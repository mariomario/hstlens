#!/bin/bash

for i in $(seq 0.2 0.025 0.55)
do 
 cat train.py | sed s/'eta_dropout = 0.6'/"eta_dropout = $i"/g > "tmptrain_$i.py"
 python "tmptrain_$i.py" > "tmptrain_$i.out"
 mv model.hd5 "tmptrain_$i.hd5"
done
