#!/bin/bash
#
#This script makes a train and a test folder with subfolders circled and untouched
#It then makes symbolic links from there to images in circled and in untouched folders
#$1 images from folder circled are linked from train/circled and the rest from test/circled
#$2 images from folder untouched are linked from train/untouched and the rest from test/untouched
#In other words
#$1 should be number of circled images to include in train
#$2 number of untouched images to include in train
#$1 should be < number of files in circled
#$2 should be < number of files in untouched
# e.g.
# ./split.sh 1190 10710 #puts 70% in train, 30% in test

rm -rf train test *d.tmp
mkdir train
mkdir train/circled
mkdir train/untouched
mkdir test
mkdir test/circled
mkdir test/untouched

ls circled | shuf > circled.tmp
ls untouched | shuf > untouched.tmp

#creating train folders
for nome in $(cat circled.tmp | head -n $1)
do
    ln -s $(pwd)/circled/$nome $(pwd)/train/circled/$nome
done
for nome in $(cat untouched.tmp | head -n $2)
do 
    ln -s $(pwd)/untouched/$nome $(pwd)/train/untouched/$nome
done

#creating test folders with the remaining files
a=$(echo "$(ls circled | wc --l) - $1" | bc)
b=$(echo "$(ls untouched | wc --l) - $2" | bc)

for nome in $(cat circled.tmp | tail -n $a)
do
    ln -s $(pwd)/circled/$nome $(pwd)/test/circled/$nome
done
for nome in $(cat untouched.tmp | tail -n $b)
do 
    ln -s $(pwd)/untouched/$nome $(pwd)/test/untouched/$nome
done



rm -rf *d.tmp
