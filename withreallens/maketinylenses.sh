#!/bin/bash

convert -crop '100x100+110+110' lens.jpg tinylens0.jpg
convert -crop '100x100+110+110' -flip lens.jpg tinylens1.jpg
convert -crop '100x100+110+110' -flop lens.jpg tinylens2.jpg
convert -crop '100x100+110+110' -flip -flop lens.jpg tinylens3.jpg
convert -crop '100x100+110+110' -resize 50% lens.jpg tinylens4.jpg
convert -crop '100x100+110+110' -resize 50% -flip lens.jpg tinylens5.jpg
convert -crop '100x100+110+110' -resize 50% -flop lens.jpg tinylens6.jpg
convert -crop '100x100+110+110' -resize 50% -flop -flip lens.jpg tinylens7.jpg
convert -crop '100x100+110+110' -resize 25% lens.jpg tinylens8.jpg
convert -crop '100x100+110+110' -resize 25% -flip lens.jpg tinylens9.jpg
convert -crop '100x100+110+110' -resize 25% -flop lens.jpg tinylens10.jpg
convert -crop '100x100+110+110' -resize 25% -flop -flip lens.jpg tinylens11.jpg

