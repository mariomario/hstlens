from keras.models import load_model
import matplotlib.image as mpimg
import numpy as np
import os

model = load_model('model.hd5')

def imgprob(filename):
    img = mpimg.imread(filename)
    img.shape = [1,512,512,3] #need to do this to feed it into the model
    img = img.astype('float32')
    img = img/255 #also this, to go from integers to [0,1]
    p = model.predict(img, verbose = 1)
    return p

def predictimg(filename):
    p = imgprob(filename)
    predicted_class_y = np.argmax(p)
    return predicted_class_y

def list_test_files():
    test_files = []
    for dirname, dirnames, filenames in os.walk('test'):
        for filename in filenames:
            test_files.append(os.path.join(dirname, filename))
    return(test_files)

legenda = ["Con cerchietto", "Senza cerchietto"]
cerchiati = 0
noncerchiati = 0 
for test_file in list_test_files():
    predicted = predictimg(test_file)
    noncerchiati += predicted
    cerchiati += (1 - predicted)
    print test_file + " " + legenda[predicted]

print "Predicted Con cerchietto " + str(cerchiati)
print "Predicted Senza cerchietto " + str(noncerchiati)
