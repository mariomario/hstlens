#!/bin/bash

folder=$1
mkdir -p $folder/balanced
for nome in $(ls $folder/untouched | shuf | head -n $(ls $folder/circled | wc -l))
do
 mv $folder/untouched/$nome $folder/balanced/
done
rm -rf $folder/untouched
mv $folder/balanced $folder/untouched
